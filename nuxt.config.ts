// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  css: ['~/assets/scss/normalize.scss', '~/assets/scss/main.scss'],
  app: {
    baseURL:"/tournee_retourne/",
    pageTransition: { name: 'page', mode: 'out-in' },
    head: {
      title: 'Retoune Tour 2024',
      link: [
        { rel: 'icon', type: 'image/png', href: '/favicon-16x16.png' }
      ],
      htmlAttrs: {
        lang: 'fr',
      },
      meta :[
        {name:'description',content :"Retoune Tour, du 3 au 11 Mai 2024,  Undae Tropic et Dezeffe joueront à Caen, Saint Etienne, Dijon et Vaulx."},
        {name:'keywords',content :"tournée, Caen, Saint Etienne, musique, 2024, Dezeffe, Undae Tropic"},
      ],
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
    }
  }
})
